﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WalidacjaRAD
{
    public partial class MainWindow : Form
    {
        List<string> listViewItems = new List<string>();
        const string dbFilePath = "Db.txt";

        public MainWindow()
        {
            InitializeComponent();
            CardValidation.GetNumersFromFile();
            textBoxPath.ReadOnly = true;
            RefreshList();
        }

        private void RefreshList()
        {
            listViewItems = new List<string>();
            listBox1.Items.Clear();
            if (!File.Exists(dbFilePath))
            {
                using (StreamWriter w = File.AppendText(dbFilePath)) ;
            }
            string[] lines = File.ReadAllLines(dbFilePath);
            foreach (string line in lines)
            {
                listViewItems.Add(line);
                listBox1.Items.Add(line);
            }
        }

        private void labelValidNumbers_Click(object sender, EventArgs e)
        {

        }

        private void AddToList(string number)
        {
            if(!listViewItems.Contains(number))
            {
                TextWriter tsw = new StreamWriter(dbFilePath, true);
                tsw.WriteLine(number);
                tsw.Close();
            }
            RefreshList();
        }

        private void DeleteFromList(string number)
        {
            var file = new List<string>(System.IO.File.ReadAllLines(dbFilePath));
            file.Remove(number);
            if(file.Count == 0)
            {
                File.WriteAllText(dbFilePath, String.Empty);
            }

            using (StreamWriter newTask = new StreamWriter(dbFilePath, false))
            {
                foreach (var fileLine in file)
                {
                    newTask.WriteLine(fileLine);
                }
            }

            RefreshList();
        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
            if(CardValidation.Validate(textBoxNumber.Text))
            {
                labelValidationResult.Text = "Numer jest prawidłowy";
                AddToList(textBoxNumber.Text);
            }
            else
            {
                labelValidationResult.Text = "Numer jest nieprawidłowy";
            }
        }

        private void buttonPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                textBoxPath.Text = dialog.FileName;
                textBoxNumber.Text = File.ReadAllText(dialog.FileName);
                long n;
                bool isNumeric = long.TryParse(textBoxNumber.Text, out n);
                if(!isNumeric)
                {
                    labelValidationResult.Text = "Numer ze ścieżki jest nieprawidłowy";
                    textBoxPath.Text = "";
                    textBoxNumber.Text = "";
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            string n = (string)listBox1.SelectedItem;
            DeleteFromList(n);
        }

        private void textBoxNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            {
                e.Handled = true;
            }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }
    }
}
