﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WalidacjaRAD
{
    class CardValidation
    {
        public static void GetNumersFromFile()
        {

            return;
        }

        public static bool Validate(string number)
        {
            int numberSum = 0;
            List<int> numbers = new List<int>();
            foreach(var c in number)
            {
                numbers.Add(c - '0');
            }
            for(int i = 0; i< numbers.Count(); i++)
            {
                if(i % 2 == 0)
                {
                    numbers[i] = numbers[i] * 2;
                    if(numbers[i] >= 10 && numbers[i] <= 99)
                    {
                        int sum = 0;
                        while (numbers[i] != 0)
                        {
                            sum += numbers[i] % 10;
                            numbers[i] /= 10;
                        }
                        numbers[i] = sum;
                    }
                }
                numberSum += numbers[i];
            }
            if (numberSum % 10 == 0 && numberSum != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
